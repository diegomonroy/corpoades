<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_page( array( 'inicio' ) ) ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_donacion' ); endif; ?>
				<?php if ( is_page( array( 'proyectos' ) ) ) : dynamic_sidebar( 'banner_proyectos' ); endif; ?>
				<?php if ( is_page( array( 'contacto' ) ) ) : dynamic_sidebar( 'banner_contacto' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->
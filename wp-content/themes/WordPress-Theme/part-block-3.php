<?php
$style = '';
if ( is_page( array( 'inicio' ) ) ) :
	$style = 'home';
endif;
?>
<!-- Begin Block 3 -->
	<section class="block_3 <?php echo $style; ?>" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_3' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 3 -->
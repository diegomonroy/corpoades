<!-- Begin Block 1 Home -->
	<section class="block_1_home" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_1_home' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 1 Home -->
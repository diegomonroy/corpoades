<!-- Begin Block 2 Home -->
	<section class="block_2_home" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_2_home' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 2 Home -->
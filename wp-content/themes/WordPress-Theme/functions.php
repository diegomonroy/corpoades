<?php

/*

Functions for my template

*/

/*
 * Function to add my styles files
 */
function my_styles_files() {
	wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/build/foundation/css/foundation.min.css', false );
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/build/animate.css/animate.min.css', false );
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/build/fancybox/jquery.fancybox.min.css', false );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/build/font-awesome/css/font-awesome.min.css', false );
	if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-css', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
	}
	wp_enqueue_style( 'theme-css', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'my_styles_files' );

/*
 * Function to add my scripts files
 */
function my_scripts_files() {
	//wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_files' );

/*
 * Function to add my scripts files in footer
 */
function my_scripts_files_footer() {
	//wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/build/foundation/js/vendor/jquery.js', false );
	wp_enqueue_script( 'what-input-js', get_template_directory_uri() . '/build/foundation/js/vendor/what-input.js', false );
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/build/foundation/js/vendor/foundation.min.js', false );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/build/fancybox/jquery.fancybox.min.js', false );
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/build/wow/wow.min.js', false );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/build/app.js', false );
}
add_action( 'wp_footer', 'my_scripts_files_footer' );

/*
 * Function to register my menus
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'main-menu' => __( 'Main Menu' ),
			'product-menu' => __( 'Product Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/*
 * Function to add theme support
 */
add_theme_support( 'post-thumbnails' );

/*
 * Function to register my sidebars and widgetized areas
 */
function arphabet_widgets_init() {
	register_sidebar(
		array(
			'name' => 'Top 1',
			'id' => 'top_1',
			'before_widget' => '<div class="moduletable_to11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo',
			'id' => 'logo',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Menu',
			'id' => 'menu',
			'before_widget' => '<div class="moduletable_to2">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Inicio',
			'id' => 'banner_inicio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Donación',
			'id' => 'banner_donacion',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Proyectos',
			'id' => 'banner_proyectos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Contacto',
			'id' => 'banner_contacto',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 1 - Inicio',
			'id' => 'block_1_home',
			'before_widget' => '<div class="moduletable_b11_home">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 2 - Inicio',
			'id' => 'block_2_home',
			'before_widget' => '<div class="moduletable_b21_home">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 1 - Donación',
			'id' => 'block_1',
			'before_widget' => '<div class="moduletable_b11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 2 - Donación',
			'id' => 'block_2',
			'before_widget' => '<div class="moduletable_b21">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 3',
			'id' => 'block_3',
			'before_widget' => '<div class="moduletable_b31">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 4',
			'id' => 'block_4',
			'before_widget' => '<div class="moduletable_b41">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 5',
			'id' => 'block_5',
			'before_widget' => '<div class="moduletable_b51">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 6',
			'id' => 'block_6',
			'before_widget' => '<div class="moduletable_b61">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Bottom',
			'id' => 'bottom',
			'before_widget' => '<div class="moduletable_bo1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

/*
 * Function to declare WooCommerce support
 */
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'woocommerce_support' );

/*
 * Functions to declare WooCommerce image sizes
 */
function woocommerce_catalog_image( $size ) {
	return array(
		'width' => 220,
		'height' => 220,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_thumbnail', 'woocommerce_catalog_image' );

function woocommerce_single_image( $size ) {
	return array(
		'width' => 600,
		'height' => 600,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_single', 'woocommerce_single_image' );

function woocommerce_gallery_image( $size ) {
	return array(
		'width' => 200,
		'height' => 200,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', 'woocommerce_gallery_image' );

/*
 * Function to declare WooCommerce products per page
 */
function new_loop_shop_per_page( $cols ) {
	$cols = 10;
	return $cols;
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

/*
 * Custom hooks in WooCommerce
 */
function custom_description() {
	the_content();
}
add_action( 'woocommerce_single_product_summary', 'custom_description', 10 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

function custom_clear() {
	echo '
		<div class="clear"></div>
		<div class="text-center">
			<button class="hollow button" onclick="window.history.go(-1);">Volver</button>
		</div>
	';
}
add_action( 'woocommerce_after_single_product_summary', 'custom_clear', 10 );

/*
 * Custom shortcode to PayPal form
 */
function shortcode_paypal() {
	$link = '';
	$apikey = '';
	$merchantId = '';
	$accountId = '';
	$date = date( 'Y-m-d H:i:s' );
	$currency = 'COP';
	$test = 0;
	$lng = 'es';
	$responseUrl = get_site_url() . '/paypal_response.php';
	$confirmationUrl = get_site_url() . '/paypal_confirmation.php';
	$description = 'CORPOADES';
	$referenceCode = 'CORPOADES ' . $date;
	$html = '
		<script src="' . get_site_url() . '/wp-content/themes/WordPress-Theme/build/jquery.md5.js"></script>
		<script>
			jQuery( document ).ready(function() {
				jQuery( "#amount" ).on( "change", function() {
					var amount = this.value;
					var chain = "' . $apikey . '~' . $merchantId . '~' . $referenceCode . '~" + amount + "~' . $currency . '";
					var md5 = jQuery.md5( chain );
					jQuery( "#paypal" ).append( "<input type=\"hidden\" name=\"signature\" value=\"" + md5 + "\">" );
				});
			});
		</script>
		<div class="paypal_form">
			<h6 class="text-center">NUESTRAS DONACIONES SE REALIZAN DE FORMA SEGURA POR MEDIO DE PAYPAL</h6>
			<p class="text-center">Por favor ingrese el valor a pagar (Ej: 100000):</p>
			<form id="paypal" action="' . $link . '" method="post">
				<fieldset>
					<input type="text" name="payerFullName" placeholder="Nombre" required>
					<input type="number" name="mobilePhone" placeholder="Celular" required>
					<input type="email" name="payerEmail" placeholder="Email" required>
					<input type="number" id="amount" name="amount" placeholder="Valor a Donar" required>
					<div class="text-center"><input type="submit" name="submit" value="Donar" class="button"></div>
				</fieldset>
				<input type="hidden" name="merchantId" value="' . $merchantId . '">
				<input type="hidden" name="accountId" value="' . $accountId . '">
				<input type="hidden" name="description" value="' . $description . '">
				<input type="hidden" name="referenceCode" value="' . $referenceCode . '">
				<input type="hidden" name="tax" value="0">
				<input type="hidden" name="taxReturnBase" value="0">
				<input type="hidden" name="currency" value="' . $currency . '">
				<input type="hidden" name="lng" value="' . $lng . '">
				<input type="hidden" name="test" value="' . $test . '" >
				<!--<input type="hidden" name="responseUrl" value="' . $responseUrl . '">-->
				<input type="hidden" name="confirmationUrl" value="' . $confirmationUrl . '">
				<input type="hidden" name="buttonType" value="SIMPLE">
			</form>
		</div>
	';
	return $html;
}
add_shortcode( 'sas_paypal', 'shortcode_paypal' );